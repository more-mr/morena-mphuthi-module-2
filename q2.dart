void main() {
  
  //2.Create an array to store all the winning apps of the MTN Business App of the Year Awards since 2012; 
   var winningApps = {
    2012: ["Transunion dealers guide","Rapid targets","Plascon","Phrazapp","Matchy" ,"Healthid","Fnb banking"], 
    2013: ["Snapscan","Price check","Nedbank","Markitshare","Kids aid","Gautrain-buddy","Dstv","Deloitte digital","Bookly"],
    2014: ["Zapper","Wildlife-tracker","Vigo","Sync mobile","Supersport","Rea vaya","My belongings","Live inspect"] ,
    2015: ["Wumdrop","Vula mobile","m4jam","Eskomsepush","Dstv now" ,"Cput mobile"] ,
    2016: ["iKhoha","HearZA","Tuta-me","KaChing","Friendly Math Monsters for Kindergarten"],
    2017: ["TransUnion 1Check","OrderIN","EcoSlips","InterGreatMe","Zulzi","Hey Jude","Oru Social","TouchSA","Pick n Pay Super Animals 2" ,
    "The TreeApp South Africa" ,"WatIf Health Portal" ,"Awethu Project" ,"Shyft for Standard Bank"] ,
    2018: ["Pineapple","Cowa Bunga","Digemy Knowledge Partner and Besmarter","Bestee App","Stokfella","Difela Hymns",
    "The African Cyber Gaming League App","dbTrack","Xander","Ctrl"] ,
    2019: ["SI Realities","Lost Defence","Franc","Vula Mobile","Matric Live","My Pregnancy Journal and LocTransie " ,
    "Hydra","Bottles" ,"Over","Digger","Mo Wash"] ,
    2020:["Checkers Sixty60","Technishen","BirdPro","Lexie Hearing app","GreenFingers Mobile","Xitsonga Dictionary" ,
    "StokFella","Bottles","Matric Live","Guardian Health","Phanda Ispani","My Pregnancy Journey"] ,
    2021: ["iiDENTIFii app","Hellopay SoftPOS","Guardian Health Platform","Ambani Africa","Murimi","Shyft","Sisa",
    "UniWise","Kazi","Takealot app","Rekindle Learning app","Roadsave","Afrihost"]
   };
  
  //a) Sort and print the apps by name;
  var appsSort = [...?winningApps[2012],...?winningApps[2013],...?winningApps[2014],...?winningApps[2015],
                  ...?winningApps[2016],...?winningApps[2017],...?winningApps[2018],...?winningApps[2019],
                  ...?winningApps[2020],...?winningApps[2021]];
  
  appsSort.sort((a, b) => a.toUpperCase().compareTo(b.toUpperCase()));
  
  for(var appName in appsSort){
    print("$appName");
  }
  
  //b) Print the winning app of 2017 and the winning app of 2018.; (
  var apps17_18 = [...?winningApps[2017],...?winningApps[2018]];
  
  for(var appName in apps17_18){
    print("$appName");
  }

 
  //c) the Print total number of apps from the array
  print(appsSort.length);
}
