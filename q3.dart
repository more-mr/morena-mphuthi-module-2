//Create a class and 
class App{
    
    String? _name;
    String? _category;
    String? _developer;
    int? _year;
    
 
    App(String name, String category, String developer, int year){
      
      _name = name;
      _category = category;
      _developer = developer;
      _year = year;
    }
    
    //b) Create a function inside the class, transform the app name to all capital letters and then print the output.
    void appTransform(){
      var appNameCaps = _name?.toUpperCase();
      print("$appNameCaps");
    }
  }
  



void main(){  
  //a) then use an object to print the name of the app, sector/category, developer and the year it won Awards.
  var obj = App("Checkers Sixty60", "Best Enterprise Solution", "Checkers", 2020);
  obj.appTransform();
}
